/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twitterdesktop;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author JordyVillao
 */
public class TwiitterJavillao extends Application {
    
    public static Stage stage = new Stage();
    public static Usuario actualUsuario = null;
    public static ArrayList<Usuario> usuarios = new ArrayList();
    public static ScreensController mainContainer = new ScreensController();
    
    public static void resizeScreen(){
		stage.sizeToScene();
		stage.centerOnScreen();
    }
    public static void replaceScreen(String nameScreen, FXMLLoader loader){
        mainContainer.unloadScreen(nameScreen);
        mainContainer.loadScreen(nameScreen, loader.getLocation().getFile());
    }
    @Override
    public void start(Stage stage) throws Exception {
        this.stage.setOnCloseRequest(e -> {
            saveUsers();
        });
        mainContainer.loadScreen("login", "Login.fxml");
        mainContainer.loadScreen("verification","Verification.fxml");
        mainContainer.loadScreen("twitter","TwitterScreen.fxml");
        
        mainContainer.setScreen("login");
        Group root = new Group();
        //Parent root = FXMLLoader.load(getClass().getResource("FXMLLogin.fxml"));
        root.getChildren().addAll(mainContainer);
        Scene scene = new Scene(root);
        this.stage.setScene(scene);
        this.stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        loadUsers();
        launch(args);
    }
    
    public static void loadUsers()
    {
        try(ObjectInputStream input = new ObjectInputStream( new FileInputStream("usuarios.bin")))
        {
            usuarios = (ArrayList<Usuario>) input.readObject();
            
        }
        catch(Exception e)
        {
            System.out.println("No hay usuarios almacenados.");
            //e.printStackTrace();
        }
    }
    public static void saveUsers()
    {
        try(ObjectOutputStream output = new ObjectOutputStream( new FileOutputStream("usuarios.bin")))
        {
            output.writeObject(usuarios);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
