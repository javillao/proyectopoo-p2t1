/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twitterdesktop;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.TwitterException;

/**
 * FXML Controller class
 *
 * @author JordyVillao
 */
public class TwitterScreenController implements Initializable,ControlledScreen {
    ScreensController myController;
    
    @FXML private TextField txtSearchTweets;
    @FXML private TextArea txtAreaNewTweet;
    @FXML private ChoiceBox<Integer> choiceBoxMinutes;
    @FXML private ListView<Status> listViewTweets = new ListView();
    @FXML private Button btnSearchTweets;
    @FXML private Button btnShowTimeline;
    @FXML private Button btnTwittear;
    @FXML private Button btnRetwitter;
    @FXML private Button btnShowSavedTweets;
    @FXML private Button btnSaveTweets;
    @FXML private Label lblBtnShowTimeline;
    @FXML private Label lblBtnSearchTweets;
    static boolean tweetSent = false;
    static int actualMin = 0;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        try{
        txtAreaNewTweet.setWrapText(true);
        listViewTweets.setCellFactory(p -> new TweetCell());
        txtSearchTweets.setPromptText("Buscar tweeets por tema de interes...");
        choiceBoxMinutes.getItems().addAll(0,5,10,15,20,25,30);
        choiceBoxMinutes.getSelectionModel().select(0);
        
        btnSaveTweets.setVisible(false);
        btnSaveTweets.setMouseTransparent(true);
        
        ImageView img1 = new ImageView(new Image("/icons/searchIcon.png"));
        img1.setFitHeight(15);
        img1.setFitWidth(15);
        btnSearchTweets.setGraphic(img1);
        
        ImageView img2 = new ImageView(new Image("/icons/refreshIcon.png"));
        img2.setFitHeight(15);
        img2.setFitWidth(15);
        btnShowTimeline.setGraphic(img2);
        
        lblBtnShowTimeline.setStyle("-fx-background-color: #ffffff;"+ "-fx-border-color: black;" + "-fx-border-width: 1;");
        lblBtnShowTimeline.setText("Mostrar timeline");
        lblBtnShowTimeline.setVisible(false);
        
        lblBtnSearchTweets.setStyle("-fx-background-color: #ffffff;"+ "-fx-border-color: black;" + "-fx-border-width: 1;");
        lblBtnSearchTweets.setText("Buscar");
        lblBtnSearchTweets.setVisible(false);
        
        
        Thread threadRefreshTimeline = new Thread( new updatingThread());
        threadRefreshTimeline.start();
        }catch(Exception ex){
            
        }
        
    }    

    @Override
    public void setScreenParent(ScreensController screenPage) {
        this.myController = screenPage;
    }
    
    @FXML
    public void showTimeline(ActionEvent evt){
        
        if(TwiitterJavillao.actualUsuario != null){
            listViewTweets.getItems().clear();
            txtSearchTweets.setText("");
            try {
                Paging paging = new Paging();
                paging.setCount(30);
                ResponseList<Status> tweets = TwiitterJavillao.actualUsuario.getTwitter().getHomeTimeline(paging);
                for(Status s : tweets){
                    listViewTweets.getItems().add(s);
                }
            } catch (TwitterException ex) {
                
            }
        }
    }
    
    @FXML 
    public void searchTweets(ActionEvent evt){
        if(!txtSearchTweets.getText().isEmpty())
        {
            Query query = new Query(txtSearchTweets.getText());
            try {
                QueryResult result = TwiitterJavillao.actualUsuario.getTwitter().search(query);
                listViewTweets.getItems().clear();
                for(Status s : result.getTweets())
                    listViewTweets.getItems().add(s);
            
            } catch (TwitterException ex) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION,"No se han encontrado coincidencias.",ButtonType.OK);
                alert.showAndWait();
                if(alert.getResult() == ButtonType.OK)
                    alert.close();
            }
        } 
    }
    
    @FXML 
    public void retwittear(ActionEvent evt){
        if(listViewTweets.getSelectionModel().getSelectedItems().size() != 0)
        {
            for(int i = 0; i<listViewTweets.getSelectionModel().getSelectedItems().size() ; i++)
            {
                long tweetId = listViewTweets.getSelectionModel().getSelectedItems().get(i).getId();
                try {
                    TwiitterJavillao.actualUsuario.getTwitter().retweetStatus(tweetId);
                    Alert alert = new Alert(Alert.AlertType.INFORMATION,"Retweet realizado con exito!.",ButtonType.OK);
                    alert.showAndWait();
                     if(alert.getResult() == ButtonType.OK)
                        alert.close();
                } catch (TwitterException ex) {
                    Alert alert = new Alert(Alert.AlertType.ERROR,"No se pudo realizar el retweet.",ButtonType.OK);
                    alert.showAndWait();
                     if(alert.getResult() == ButtonType.OK)
                        alert.close();
                }
            }
        }
        else
        {
            Alert alert = new Alert(Alert.AlertType.WARNING,"No ha seleccionado ningun tweet.",ButtonType.OK);
            alert.showAndWait();
            if(alert.getResult() == ButtonType.OK)
               alert.close();
        }
    }
    
    @FXML 
    public void twittear(ActionEvent evt){
        if(!txtAreaNewTweet.getText().isEmpty() && choiceBoxMinutes.getSelectionModel().getSelectedItem() == 0){
            try {
                Status tweet = TwiitterJavillao.actualUsuario.getTwitter().updateStatus(txtAreaNewTweet.getText());
                Alert alert = new Alert(Alert.AlertType.INFORMATION,"Tweet realizado con exito.",ButtonType.OK);
                alert.showAndWait();
                if(alert.getResult() == ButtonType.OK)
                    alert.close();
            } catch (Exception ex) {
                if(txtAreaNewTweet.getText().length()>280){
                        Alert alert = new Alert(Alert.AlertType.ERROR,"No se pudo retwittear. Se excede el numero de caracteres",ButtonType.OK);
                        alert.showAndWait();
                        if(alert.getResult() == ButtonType.OK)
                            alert.close();
                }else{
                    Thread t = new Thread(new Runnable(){
                        @Override
                        public void run() {
                            tweetSent = true;
                            while(tweetSent){
                                Platform.runLater(new Runnable(){
                                    @Override
                                    public void run() {
                                        try {
                                            Status tweet = TwiitterJavillao.actualUsuario.getTwitter().updateStatus(txtAreaNewTweet.getText());
                                            tweetSent = false;
                                        } catch (TwitterException ex1) {
                                        }
                                    }
                                });
                                try {
                                    Thread.sleep(1000*30);
                                } catch (InterruptedException ex1) {
                                    Logger.getLogger(TwitterScreenController.class.getName()).log(Level.SEVERE, null, ex1);
                                }
                            }
                        }  
                    });
                    t.start();
                }
            }
        }else if(!txtAreaNewTweet.getText().isEmpty() && choiceBoxMinutes.getSelectionModel().getSelectedItem() != 0){
            actualMin = choiceBoxMinutes.getSelectionModel().getSelectedItem();
            Thread t = new Thread(new Runnable(){
               @Override
               public void run(){
                    try{
                       Thread.sleep(1000*actualMin);
                    } catch (InterruptedException ex) {
                       Logger.getLogger(TwitterScreenController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    Platform.runLater(new Runnable(){
                        @Override
                        public void run(){
                            try{
                                Status tweet = TwiitterJavillao.actualUsuario.getTwitter().updateStatus(txtAreaNewTweet.getText());
                                Alert alert = new Alert(Alert.AlertType.INFORMATION,"Tweet realizado con exito.",ButtonType.OK);
                                alert.showAndWait();
                                if(alert.getResult() == ButtonType.OK)
                                    alert.close();
                            }catch (TwitterException ex) {
                                Alert alert = new Alert(Alert.AlertType.ERROR,"Error al realizar el tweet.",ButtonType.OK);
                                alert.showAndWait();
                                if(alert.getResult() == ButtonType.OK)
                                    alert.close();
                            }
                        }
                    });
               }
            });
        }
    }
    
    @FXML 
    public void saveTweets(ActionEvent evt){
        System.out.println(listViewTweets.getSelectionModel().getSelectedItems().size());
//        if(listViewTweets.getSelectionModel().getSelectedItems().size()!= 0){
//            for(Status s : listViewTweets.getSelectionModel().getSelectedItems())
//                TwiitterJavillao.actualUsuario.getTweetsGuardados().add(s);
//            TwiitterJavillao.usuarios.remove(TwiitterJavillao.actualUsuario);
//            TwiitterJavillao.usuarios.add(TwiitterJavillao.actualUsuario);
//        }
    }
    
    @FXML 
    public void showSavedTweets(ActionEvent evt){
        if(netIsAvailable()){
            if(TwiitterJavillao.actualUsuario.getTweetsGuardados().size()!=0){
                listViewTweets.getItems().clear();
                for(Status s : TwiitterJavillao.actualUsuario.getTweetsGuardados())
                listViewTweets.getItems().add(s);      
            }else{
                Alert alert = new Alert(Alert.AlertType.INFORMATION,"No ha guardado tweets.",ButtonType.OK);
                alert.showAndWait();
                if(alert.getResult() == ButtonType.OK)
                   alert.close();
            } 
        }else{
            if(TwiitterJavillao.actualUsuario.getTweetsGuardados().size()!=0){
                VBox root = new VBox();
                ListView<PseudoStatus> statusGuardados = new ListView();
                statusGuardados.setCellFactory(e -> new PseudoTweetCell());
                for(PseudoStatus p : TwiitterJavillao.actualUsuario.getStatusGuardados())
                    statusGuardados.getItems().add(p);
                root.getChildren().add(statusGuardados);
                Stage newStage = new Stage();
                newStage.setScene(new Scene(root,500,500));
                newStage.showAndWait();
            }else{
                Alert alert = new Alert(Alert.AlertType.INFORMATION,"No ha guardado tweets.",ButtonType.OK);
                alert.showAndWait();
                if(alert.getResult() == ButtonType.OK)
                   alert.close();
            } 
        }
        
    }
    
    @FXML
    public void showLabelBtnShowTimeline(MouseEvent evt){
         lblBtnShowTimeline.setVisible(true);
    }
    
    @FXML
    public void hideLabelBtnShowTimeline(MouseEvent evt){
         lblBtnShowTimeline.setVisible(false);
    }
    
    @FXML
    public void showLabelBtnSearchTweets(MouseEvent evt){
         lblBtnSearchTweets.setVisible(true);
    }
    
    @FXML
    public void hideLabelBtnSearchTweets(MouseEvent evt){
         lblBtnSearchTweets.setVisible(false);
    }
    
    public class updatingThread implements Runnable{

        @Override
        public void run() {
            while(true){
                Platform.runLater(new Runnable(){
                    public void run(){
                        try{
                            if(txtSearchTweets.getText().isEmpty()){
                            showTimeline(new ActionEvent());
                            //System.out.println("Actualizado.");
                        }
                        }catch(Exception ex){
                            
                        }
                        
                    }
                });
                try{
                    Thread.sleep(1000*30);
                } catch (InterruptedException ex) {
                    Logger.getLogger(TwitterScreenController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex){
                        System.out.println("Exception.");
                }
            }
        }
        
    }
    
    private static boolean netIsAvailable(){
        try {
            final URL url = new URL("http://www.google.com");
            final URLConnection conn = url.openConnection();
            conn.connect();
            conn.getInputStream().close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}
