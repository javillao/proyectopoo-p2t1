/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twitterdesktop;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * FXML Controller class
 *
 * @author JordyVillao
 */
public class VerificationController implements Initializable, ControlledScreen {
    ScreensController myController;
    
    @FXML private Label lblErrorPin;
    @FXML private TextField txtPinVerification;
    @FXML private Button btnAuthenticate;
    @FXML private WebView webViewTwitter;
    private Twitter twitter;
    private RequestToken requestToken;
    public static String urlAuthorization;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try{
            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setDebugEnabled(true)
            .setOAuthConsumerKey("RVfjqQJNBL6hwoYr57ubbKK1l")
            .setOAuthConsumerSecret("aloF0zCPIA9vtz7iJnWqJIqDcvOJer2xZ0LxWrFa5Ye7CAI39F");
            Configuration configuration = builder.build();
            TwitterFactory factory = new TwitterFactory(configuration);
            twitter = factory.getInstance();
            requestToken = null;

            try {
                requestToken = twitter.getOAuthRequestToken();
                urlAuthorization = requestToken.getAuthorizationURL();
                System.out.println(urlAuthorization);
                WebEngine engine = webViewTwitter.getEngine();
                engine.load(urlAuthorization);
            } catch (TwitterException ex) {
                //ex.printStackTrace();
            }
        }catch(Exception e){}
    }    

    @Override
    public void setScreenParent(ScreensController screenPage) {
        this.myController = screenPage;
    }
    
    @FXML
    public void openTwitter(ActionEvent evt){
        try{
            System.out.println("Entro");
            String pin = txtPinVerification.getText();
            AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, pin);
            System.out.println("Llego");
            TwiitterJavillao.actualUsuario = new Usuario(accessToken);
            TwiitterJavillao.usuarios.add(TwiitterJavillao.actualUsuario);
            lblErrorPin.setText("Acceso validado!");
            
//            FXMLLoader loader = new FXMLLoader(getClass().getResource("TwitterScreen.fxml"));
//            TwitterScreenController controller = loader.getController();
//            controller.showTimeline(evt);
//            TwiitterJavillao.replaceScreen("twitter", loader);
 
            myController.setScreen("twitter");

        }catch(TwitterException ex){
            if(ex.getStatusCode()== 401){               
                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.setDebugEnabled(true)
                .setOAuthConsumerKey("RVfjqQJNBL6hwoYr57ubbKK1l")
                .setOAuthConsumerSecret("aloF0zCPIA9vtz7iJnWqJIqDcvOJer2xZ0LxWrFa5Ye7CAI39F");
                Configuration configuration = builder.build();
                twitter = new TwitterFactory(configuration).getInstance();
                requestToken = null;
                try {
                    requestToken = twitter.getOAuthRequestToken();
                    urlAuthorization = requestToken.getAuthorizationURL();
                    System.out.println(urlAuthorization);
                    WebEngine engine = webViewTwitter.getEngine();
                    engine.load(urlAuthorization);
                } catch (TwitterException ex2) {
                    ex.printStackTrace();
                }
                
                WebEngine engine = webViewTwitter.getEngine();
                engine.load(urlAuthorization);
                //ex.printStackTrace();
                lblErrorPin.setText("Error en la autenticacion!!");
                lblErrorPin.setStyle("-fx-color: red");
            }
        }
        
    }
    
}
